package es.iesnervion.migue.prueba_vc_fragmentsvm;

import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;


public class MainActivity extends AppCompatActivity {

    private MainViewModel vm;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        vm = ViewModelProviders.of(this).get(MainViewModel.class);

        final boolean smallScreen;
        Fragment fragmentOpc = new FragmentOptions();
        final Fragment fragmentForm = new FragmentForm();
        final Fragment fragmentList = new FragmentListaPersonas();
        //If R.id.contenedorMovil != null, it means the device
        //has a small screen and the system loaded the
        //layout for small screens
        if (findViewById(R.id.contenedorMovil)!=null){

            smallScreen = true;
            getSupportFragmentManager().beginTransaction().replace(R.id.contenedorMovil,fragmentOpc).addToBackStack(null).commit();
        }
        else {
            smallScreen = false;
            getSupportFragmentManager().beginTransaction().replace(R.id.contenedorIzq,fragmentOpc).addToBackStack(null).commit();
        }

        vm.getVisualizacion().observe(this, new Observer<String>() {
            @Override
            public void onChanged(@Nullable String s) {
                if (smallScreen==true){
                    if (s.equals(getResources().getString(R.string.VISUALIZACION_FORM))){
                        getSupportFragmentManager().beginTransaction().replace(R.id.contenedorMovil,fragmentForm).addToBackStack(null).commit();
                    }
                    else if (s.equals(getResources().getString(R.string.VISUALIZACION_LISTA))){
                        getSupportFragmentManager().beginTransaction().replace(R.id.contenedorMovil,fragmentList).addToBackStack(null).commit();
                    }
                }
                else {
                    if (s.equals(getResources().getString(R.string.VISUALIZACION_FORM))){
                        //Si queremos tener los tres fragments visibles al mismo tiempo:
                        //getSupportFragmentManager().beginTransaction().replace(R.id.contenedorMed,fragmentForm).addToBackStack(null).commit();
                        getSupportFragmentManager().beginTransaction().replace(R.id.contenedorDcha,fragmentForm).addToBackStack(null).commit();
                    }
                    else if (s.equals(getResources().getString(R.string.VISUALIZACION_LISTA))){
                        getSupportFragmentManager().beginTransaction().replace(R.id.contenedorDcha,fragmentList).addToBackStack(null).commit();
                    }
                }

            }
        });

    }
}
