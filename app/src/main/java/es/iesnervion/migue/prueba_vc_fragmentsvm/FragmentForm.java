package es.iesnervion.migue.prueba_vc_fragmentsvm;


import androidx.lifecycle.ViewModelProviders;
import android.os.Bundle;
import androidx.fragment.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;


/**
 * A simple {@link Fragment} subclass.
 * Use the {@link FragmentForm#newInstance} factory method to
 * create an instance of this fragment.
 */
public class FragmentForm extends Fragment implements View.OnClickListener {
    // TODO: Rename parameter arguments, choose names that match
    //TODO: En vez de parámetros aquí, ¿los ponemos en ViewModel? => With LiveData we wouldn't need Arguments (factory method) anymore...¿?
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;


    private EditText campo;
    private MainViewModel vm;


    public FragmentForm() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment FragmentForm.
     */
    // TODO: Rename and change types and number of parameters
    public static FragmentForm newInstance(String param1, String param2) {
        FragmentForm fragment = new FragmentForm();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        vm = ViewModelProviders.of(getActivity()).get(MainViewModel.class);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v = inflater.inflate(R.layout.fragment_fragment_form, container, false);
        ((Button)v.findViewById(R.id.btn_agregar)).setOnClickListener(this);
        campo = (EditText) v.findViewById(R.id.campo);
        return v;
    }


    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.btn_agregar:
                vm.addElemento(campo.getText().toString());
                campo.setText("");
                break;

        }
    }
}
