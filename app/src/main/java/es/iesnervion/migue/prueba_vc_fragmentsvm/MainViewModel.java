package es.iesnervion.migue.prueba_vc_fragmentsvm;


import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import java.util.ArrayList;
import java.util.List;


/**
 * Created by migue on 27/01/18.
 */

public class MainViewModel extends ViewModel {


    private final MutableLiveData<String> visualizacion = new MutableLiveData<String>();
    private final MutableLiveData<ArrayList<String>> lista = new MutableLiveData<ArrayList<String>>();

    public LiveData<String> getVisualizacion() {
        return visualizacion;
    }

    public void setVisualizacion(String visualizacion) {
        this.visualizacion.setValue(visualizacion);
    }

    public LiveData<ArrayList<String>> getLista() {
        return lista;
    }

    public void addElemento(String elemento) {
        ArrayList<String> listaAux = new ArrayList<String>();
        if (this.getLista().getValue()!=null){
            listaAux = this.getLista().getValue();
        }

        listaAux.add(elemento);

        this.lista.setValue(listaAux);
    }

    public MainViewModel() {
    }


}
